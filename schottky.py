#%%cython
MAX = 200
import numba
from numba import jit
import numpy as np

m1 = np.array([[1,0],[2,1]])

# test if m.R_+ intersect R_+
@jit(nopython=True)
def intersect1 (m, verb=False):
    #cdef float a,b,c,d,A,B,C,D,t
    a = m[0][0]
    b = m[0][1]
    c = m[1][0]
    d = m[1][1]
    A = (a*c.conjugate()).imag
    B = (b*c.conjugate() + a*d.conjugate()).imag
    C = (b*d.conjugate()).imag
    #if verb:
    #    print("equation {} t**2 + {} t + {} = 0".format(A, B, C))
    if A*C >= 0:
        if A*B > 0:
            #if verb:
            #    print("min atteint avant 0")
            return False
        D = B*B - 4*A*C
        if D <= 0:
            #if verb:
            #    print("D = {} <= 0".format(D))
            return False
    #if verb:
    #    print("m.R_+ intersect R")
    # now we know that m.R_+ intersect R
    # we check if m.R intersect R_+
    m1 = np.linalg.inv(m)
    a1 = m1[0][0]
    b1 = m1[0][1]
    c1 = m1[1][0]
    d1 = m1[1][1]
    A1 = (a1*c1.conjugate()).imag
    B1 = (b1*c1.conjugate() + a1*d1.conjugate()).imag
    C1 = (b1*d1.conjugate()).imag
    if A1*C1 >= 0:
        if A1*B1 > 0:
            #if verb:
            #    print("min1 atteint avant 0")
            return False
        D1 = B1*B1 - 4*A1*C1
        if D1 <= 0:
            #if verb:
            #    print("D1 = {} <= 0".format(D1))
            return False
    # now we check if m.R_+ intersect R_+
    try: D
    except:
        D = B*B - 4*A*C
    if D <= 0:
        return False
    if A == 0:
        if B == 0:
            return False
        else:
            t = -C/B
            return t > 0 and (a*c.conjugate()*t**2 + (b*c.conjugate()+a*d.conjugate())*t+b*d.conjugate()).real > 0
    t = (-B + np.sqrt(D))/(2*A)
    #if verb:
    #    print("test si l'intersection est positive...")
    if t > 0 and (a*c.conjugate()*t**2 + (b*c.conjugate()+a*d.conjugate())*t+b*d.conjugate()).real > 0:
        return True
    t = (-B - np.sqrt(D))/(2*A)
    return t > 0 and (a*c.conjugate()*t**2 + (b*c.conjugate()+a*d.conjugate())*t+b*d.conjugate()).real > 0

# test if the point z is in the cone
@jit(nopython=True)
def is_in (z, z1):
    if z1 == np.inf:
        return False
    return (z1*z.conjugate()).imag < 0 and z1.imag > 0

# test if the lunul corresponding to m intersect the cone
@jit(nopython=True)
def intersect (m, z, verb=False):
    #cdef complex z1, z2
    if m[1][1] == 0:
        z1 = np.inf
    else:
        z1 = m[0][1]/m[1][1] # image of 0 by m
    if m[1][0] == 0:
        z2 = np.inf
    else:
        z2 = m[0][0]/m[1][0] # image of oo by m
    if verb:
        print("test if ends points are in the cone...")
    if is_in(z, z1) or is_in(z, z2):
        return True
    if verb:
        print("test if m.R_+ intersect R_+...")
    if intersect1(m):
        return True
    #m[0][1] /= z
    #m[1][1] /= z
    if verb:
        print("test if m.R_+z intersect R_+z...")
    if z == 0:
        return True
    m2 = np.array([[m[0][0], m[0][1]/z], [m[1][0]*z, m[1][1]]])
    if intersect1(m2):
        return True
    #m[0][0] /= z
    #m[1][0] /= z
    if verb:
        print("test if m.R_+ intersect R_+z...")
    m2 = np.array([[m[0][0]/z, m[0][1]/z], [m[1][0], m[1][1]]])
    if intersect1(m2):
        return True
    #m *= z
    #m[0][1] *= z
    #m[1][1] *= z
    if verb:
        print("test if m.R_+z intersect R_+...")
    m2 = np.array([[m[0][0]*z, m[0][1]], [m[1][0]*z, m[1][1]]])
    if intersect1(m2):
        return True
    return False

#@jit(nopython=False)
def schottky_rec (z, d, m, n):
    tmn = tuple((m[0][0], m[0][1], m[1][0], m[1][1], n))
    if tmn in d:
        return d[tmn]
    if n == 0:
        return -1
    if len(d) > MAX:
        return -2
    m2 = np.array([[1,z],[0,1]])
    mi = n-1
    for a in [m1, m2]:
        for b in [m1, m2]:
            m3 = np.linalg.inv(b)@m@a
            if intersect(m3, z):
                r = schottky_rec(z, d, m3, n-1)
                if r < 0:
                    d[tmn] = r
                    return r
                else:
                    if r < mi:
                        mi = r
    d[tmn] = mi
    return mi

#@jit(nopython=False)
def schottky (z, n, verb=False):
    m2 = np.array([[1,z],[0,1]])
    m = np.linalg.inv(m1)@m2
    if not intersect(m, z):
        return n
    d = dict()
    r = schottky_rec(z, d, m, n)
    d = None
    return r

W = 2000 # width of resulting image
n = 40 # maximum depht
xmin = -.2
xmax = .13
ymin = 0
ymax = .4
f = (xmax-xmin)/W
H = int(W*(ymax-ymin)/(xmax-xmin))
import numpy as np
from PIL import Image
im = np.zeros((H,W,3), dtype=np.uint8)

#@jit(nopython=True)
def g(t):
    #print(t)
    i = t[0]
    j = t[1]
    z = complex(i*f + xmin,  j*f + ymin)
    #print(z)
    global m2
    r = schottky(z, n)
    #print(r)
    if r >= 0:
        #print(" -> schottky")
        #im[j][i] = 255
        arr[j*W+i] = 255 - (n-r)*int(256/(n+1))
    elif r == -2:
        arr[j*W+i] = 1
    else:
        arr[j*W+i] = 0
    #if j == 0:
    #    print("i = {}".format(i))

#g(np.array([(i,j) for i in range(W) for j in range(H)]))
import multiprocessing
from multiprocessing import Array

arr = Array('i', range(W*H))

pool = multiprocessing.Pool()

import tqdm
from multiprocessing import Pool

pool = Pool(processes=8)
tasks = [(i,j) for i in range(W) for j in range(H)]
for _ in tqdm.tqdm(pool.imap_unordered(g, tasks), total=len(tasks)):
    pass
#pool.map(g, [(i,j) for i in range(W) for j in range(H)])

print("conversion...")
noir = np.array((0,0,0))
blanc = np.array((255,255,255))
rouge = np.array((255,0,0))
for i in range(W):
    for j in range(H):
        if arr[j*W+i] == 0:
            im[j][i] = noir
        elif arr[j*W+i] == 1:
            im[j][i] = rouge
        else:
            im[j][i] = blanc
            im[j][i][0] = arr[j*W+i]
            im[j][i][1] = arr[j*W+i]

#list(map(g, [(i,j) for i in range(W) for j in range(H)])) #np.indices((W,H))))
#np.vectorize(g)([(i,j) for i in range(W) for j in range(H)])
    
img = Image.fromarray(im, 'RGB')
img.save("schottky_{}_{}_{}_z.png".format(n, W, MAX))
#img.show()
